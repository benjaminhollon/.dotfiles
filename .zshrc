# Set up the prompt

setopt histignorealldups sharehistory

HISTSIZE=15000
SAVEHIST=15000
HISTFILE=~/.local/share/zsh/history

# Use modern completion system
autoload -Uz compinit
compinit

zstyle ':completion:*' auto-description 'specify: %d'
zstyle ':completion:*' completer _expand _complete _correct _approximate
zstyle ':completion:*' format 'Completing %d'
zstyle ':completion:*' group-name ''
zstyle ':completion:*' menu select=2
eval "$(dircolors -b)"
zstyle ':completion:*:default' list-colors ${(s.:.)LS_COLORS}
zstyle ':completion:*' list-colors ''
zstyle ':completion:*' list-prompt %SAt %p: Hit TAB for more, or the character to insert%s
zstyle ':completion:*' matcher-list '' 'm:{a-z}={A-Z}' 'm:{a-zA-Z}={A-Za-z}' 'r:|[._-]=* r:|=* l:|=*'
zstyle ':completion:*' menu select=long
zstyle ':completion:*' select-prompt %SScrolling active: current selection at %p%s
zstyle ':completion:*' use-compctl false
zstyle ':completion:*' verbose true

zstyle ':completion:*:*:kill:*:processes' list-colors '=(#b) #([0-9]#)*=0=01;31'
zstyle ':completion:*:kill:*' command 'ps -u $USER -o pid,%cpu,tty,cputime,cmd'

autoload -Uz compinit
compinit

# User specific aliases and functions
if [ -d $XDG_CONFIG_HOME/zsh/startup-scripts ]; then
	source <(find $XDG_CONFIG_HOME/zsh/startup-scripts -mindepth 1 -print0 -type f | xargs -0 cat)
fi
if [ -d $XDG_CONFIG_HOME/bash/completions ]; then
	autoload bashcompinit
	bashcompinit
	source <(find $XDG_CONFIG_HOME/bash/completions -mindepth 1 -print0 -type f | xargs -0 cat)
fi

#setopt autocd # interferes with the POSIX sh version of foot's detection of working directory

osc7_cd() {
    cd "$1" || return $?

    tmp="$PWD"
    encoded=""
    while [ -n "$tmp" ]; do
        n="${tmp#?}"
        c="${tmp%"$n"}"
        case "$c" in
            [-/:_.!\'\(\)~[:alnum:]]) encoded="$encoded$c" ;;
            *) encoded="${encoded}$(printf '%%%02X' "'$c")"  ;;
        esac
        tmp="$n"
        unset n c
    done

    printf "\033]7;file://%s%s\033\\" "$(hostname)" "$encoded"
    unset tmp encoded
}

osc7_cd "$PWD" # first-run
alias cd=osc7_cd
