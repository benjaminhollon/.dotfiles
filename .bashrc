# User specific aliases and functions
if [ -d ~/.config/bash/startup-scripts ]; then
	for rc in ~/.config/bash/startup-scripts/*; do
		if [ -f "$rc" ]; then
			. "$rc"
		fi
	done
fi

# User specific completions
if [ -d ~/.config/bash/completions ]; then
	for rc in ~/.config/bash/completions/*; do
		if [ -f "$rc" ]; then
			source "$rc"
		fi
	done
fi
