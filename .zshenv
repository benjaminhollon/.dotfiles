export GPG_TTY=$(tty)

export XDG_CONFIG_HOME=~/.config
export XDG_CACHE_HOME=~/.cache

export PATH="$HOME/.deno/bin:/usr/sbin:$PATH:$HOME/.local/bin"

source $XDG_CONFIG_HOME/zsh/git-prompt.sh
setopt PROMPT_SUBST ; export PS1='%~$(__git_ps1 " (%s)")] '

export TIMEWARRIORDB=$XDG_CONFIG_HOME/timewarrior/

export LESSHISTFILE=$XDG_CACHE_HOME/less/history
export CALCHISTFILE=$XDG_CACHE_HOME/calc/history

export BROWSER=w3m

export EDITOR=nvim

bindkey -v
bindkey '\eOH'  beginning-of-line
bindkey '\eOF'  end-of-line
bindkey '^[[3~'  delete-char

# pom
export POM_WORK_END_HOOK='notify-send "Done working; take a break!" && mpv --really-quiet "$XDG_CONFIG_HOME/sounds/applause.mp3"'
export POM_SHORT_BREAK_END_HOOK='notify-send "Break finished; get back to work!" && mpv --really-quiet "$XDG_CONFIG_HOME/sounds/applause.mp3"'
export POM_LONG_BREAK_END_HOOK=$POM_SHORT_BREAK_END_HOOK

export EMAIL="$(cat $XDG_CONFIG_HOME/email)"

export LEDGER_FILE="$(xdg-user-dir NOTES)/hledger/$(date +%Y).journal"

export ELECTRON_OZONE_PLATFORM_HINT=auto

export PAGER='less -S'
