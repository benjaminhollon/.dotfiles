include /etc/sway/config.d/*

# Startup
exec wl-paste -t text --watch clipman store
exec clipman restore
exec ~/.local/bin/sunset
exec_always doas mouseless --config /home/amin/.config/mouseless/config.yaml
exec udiskie
exec doas tlp start
exec_always doas powertop --auto-tune
exec mako
exec awatcher
exec aw-watcher-netstatus

seat seat0 xcursor_theme default 16

# Set up wob
set $WOBSOCK $XDG_RUNTIME_DIR/wob.sock
exec_always rm -f $WOBSOCK && mkfifo $WOBSOCK && tail -f $WOBSOCK | wob --config /home/amin/.config/wob/wob.ini

### Variables
#
# Logo key. Use Mod1 for Alt.
set $mod Mod4
set $duh Mod4+Mod1
# Home row direction keys, like vim
set $left h
set $down n
set $up e
set $right i
# Your preferred terminal emulator
set $term foot
# Your preferred application launcher
# Note: pass the final command to swaymsg so that the resulting window can be opened
# on the original workspace that the command was run on.
set $menu rofi -show drun -show-icons | xargs swaymsg exec --

bindsym $duh+e exec udiskie-umount -ad
bindsym $mod+u exec ~/.local/bin/umount-drive
bindsym $duh+s exec tomb slam --sudo doas all
bindsym {
	Mod1+b exec acpi | cut -d ' ' -f 4 | cut -d '%' -f 1 > $WOBSOCK

	$mod+Shift+f exec ~/.local/bin/power-menu

	# Notifications
	$mod+slash exec makoctl dismiss
	$mod+shift+slash exec makoctl restore

	Mod1+t exec wtype "$(date --iso-8601=seconds)"

	# Clipman
	$mod+semicolon exec clipman pick -t rofi
	$mod+shift+semicolon exec clipman clear -t rofi

	# Word Sprint
	$mod+equal exec $term ~/.local/bin/sprint

	# File browser (If this uses the wrong program, fix it using mimeopen)
	$mod+o exec rofi -show filebrowser

	# Emoji picker
	$mod+y exec cat "$XDG_CONFIG_HOME/emoji-list" | rofi -dmenu -i -p 'emoji' | cut -d ' ' -f 1 | tr -d '\n' | wtype -

	# Screenshot
	Print exec ~/.local/bin/screenshot copy
	Shift+Print exec ~/.local/bin/screenshot save

	# Quick note
	$mod+backslash exec ~/.local/bin/quicknote

	# Applause
	$mod+period exec mpv ~/.config/sounds/applause.mp3

	# Do Not Disturb
	XF86AudioMedia exec ~/.local/bin/toggle-do-not-disturb

	# Bluetooth devices
	$mod+p exec bluetoothctl power on && /bin/sh -c "bluetoothctl connect $(bluetoothctl devices | rofi -dmenu -i -p 'bluetooth device' | cut -d ' ' -f 2)"

	# Lock
	$mod+q exec ~/.local/bin/lock

	# Start a terminal
	$mod+Return exec $term
	$mod+Escape exec $term

	# start qutebrowser
	$mod+Shift+w exec qutebrowser
	$mod+Shift+p exec qutebrowser --target private-window

	# Kill focused window
	$mod+g kill

	# Start your launcher
	$mod+s exec $menu

	$mod+Shift+c reload

	# Exit sway (logs you out of your Wayland session)
	$mod+Shift+x exec swaymsg exit
#
# Moving around:
#
	# Move your focus around
	$mod+$left focus left
	$mod+$down focus down
	$mod+$up focus up
	$mod+$right focus right
	# Or use $mod+[up|down|left|right]
	$mod+Left focus left
	$mod+Down focus down
	$mod+Up focus up
	$mod+Right focus right

	# Move the focused window with the same, but add Shift
	$mod+Shift+$left move left
	$mod+Shift+$down move down
	$mod+Shift+$up move up
	$mod+Shift+$right move right
	# Ditto, with arrow keys
	$mod+Shift+Left move left
	$mod+Shift+Down move down
	$mod+Shift+Up move up
	$mod+Shift+Right move right
#
# Workspaces:
#
	# Switch to workspace
	$mod+1 workspace number 1
	$mod+2 workspace number 2
	$mod+3 workspace number 3
	$mod+4 workspace number 4
	$mod+5 workspace number 5
	$mod+6 workspace number 6
	$mod+7 workspace number 7
	$mod+8 workspace number 8
	$mod+9 workspace number 9
	$mod+0 workspace number 10
	$mod+KP_1 workspace number 1
	$mod+KP_2 workspace number 2
	$mod+KP_3 workspace number 3
	$mod+KP_4 workspace number 4
	$mod+KP_5 workspace number 5
	$mod+KP_6 workspace number 6
	$mod+KP_7 workspace number 7
	$mod+KP_8 workspace number 8
	$mod+KP_9 workspace number 9
	$mod+KP_0 workspace number 10
	$mod+F1 workspace 11
	$mod+F2 workspace 12
	$mod+F3 workspace 13
	$mod+F4 workspace 14
	$mod+F5 workspace 15
	$mod+F6 workspace 16
	$mod+F7 workspace 17
	$mod+F8 workspace 18
	$mod+F9 workspace 19
	$mod+F10 workspace 20
	$mod+F11 workspace 21
	$mod+F12 workspace 22
	# Move focused container to workspace
	$mod+Shift+1 move container to workspace number 1
	$mod+Shift+2 move container to workspace number 2
	$mod+Shift+3 move container to workspace number 3
	$mod+Shift+4 move container to workspace number 4
	$mod+Shift+5 move container to workspace number 5
	$mod+Shift+6 move container to workspace number 6
	$mod+Shift+7 move container to workspace number 7
	$mod+Shift+8 move container to workspace number 8
	$mod+Shift+9 move container to workspace number 9
	$mod+Shift+0 move container to workspace number 10
	$mod+Shift+KP_1 move container to workspace number 1
	$mod+Shift+KP_2 move container to workspace number 2
	$mod+Shift+KP_3 move container to workspace number 3
	$mod+Shift+KP_4 move container to workspace number 4
	$mod+Shift+KP_5 move container to workspace number 5
	$mod+Shift+KP_6 move container to workspace number 6
	$mod+Shift+KP_7 move container to workspace number 7
	$mod+Shift+KP_8 move container to workspace number 8
	$mod+Shift+KP_9 move container to workspace number 9
	$mod+Shift+KP_0 move container to workspace number 10
	$mod+Shift+F1 move container to workspace 11
	$mod+Shift+F2 move container to workspace 12
	$mod+Shift+F3 move container to workspace 13
	$mod+Shift+F4 move container to workspace 14
	$mod+Shift+F5 move container to workspace 15
	$mod+Shift+F6 move container to workspace 16
	$mod+Shift+F7 move container to workspace 17
	$mod+Shift+F8 move container to workspace 18
	$mod+Shift+F9 move container to workspace 19
	$mod+Shift+F10 move container to workspace 20
	$mod+Shift+F11 move container to workspace 21
	$mod+Shift+F12 move container to workspace 22
	# Move to left or right workspace
	$duh+h workspace prev
	$duh+i workspace next

# Layout stuff:
#
	# You can "split" the current object of your focus with
	# $mod+b or $mod+v, for horizontal and vertical splits
	# respectively.
	$mod+c splith
	$mod+v splitv

	# Switch the current container between different layout styles
	$mod+k layout stacking
	$mod+w layout tabbed
	$mod+x layout toggle split

	# Make the current focus fullscreen
	$mod+f fullscreen

	# Toggle the current focus between tiling and floating mode
	$mod+space floating toggle

	# Swap focus between the tiling area and the floating area
	$mod+Shift+space focus mode_toggle

	# Move focus to the parent container
	$mod+a focus parent
#
# Scratchpad:
#
	# Sway has a "scratchpad", which is a bag of holding for windows.
	# You can send windows there and get them back later.

	# Move the currently focused window to the scratchpad
	$mod+minus move scratchpad

	# Show the next scratchpad window or hide the focused scratchpad window.
	# If there are multiple scratchpad windows, this command cycles through them.
	$mod+Shift+minus scratchpad show
}

bindsym --locked {
	# Audio
	XF86AudioLowerVolume exec amixer sset Master 5%- | sed -En 's/.*\[([0-9]+)%\].*/\1/p' | head -1 > $WOBSOCK
	Mod1+n exec amixer sset Master 5%- | sed -En 's/.*\[([0-9]+)%\].*/\1/p' | head -1 > $WOBSOCK
	XF86AudioRaiseVolume exec amixer sset Master 5%+ | sed -En 's/.*\[([0-9]+)%\].*/\1/p' | head -1 > $WOBSOCK
	Mod1+e exec amixer sset Master 5%+ | sed -En 's/.*\[([0-9]+)%\].*/\1/p' | head -1 > $WOBSOCK
	XF86AudioMute exec amixer set Master 1+ toggle | sed -En '/\[on\]/ s/.*\[([0-9]+)%\].*/\1/ p; /\[off\]/ s/.*/0/p' | head -1 > $WOBSOCK
	Mod1+m exec amixer set Master 1+ toggle | sed -En '/\[on\]/ s/.*\[([0-9]+)%\].*/\1/ p; /\[off\]/ s/.*/0/p' | head -1 > $WOBSOCK
	Mod1+shift+m exec $term pulsemixer

	# Media
	XF86AudioPlay exec playerctl play-pause
	Mod1+space exec playerctl play-pause
	Mod1+shift+space exec ~/.local/bin/notify-playing
	XF86AudioPrev exec playerctl previous && sleep 0.5 && ~/.local/bin/notify-playing
	Mod1+h exec playerctl previous && sleep 0.5 && ~/.local/bin/notify-playing
	XF86AudioNext exec playerctl next && sleep 0.5 && ~/.local/bin/notify-playing
	Mod1+i exec playerctl next && sleep 0.5 && ~/.local/bin/notify-playing
	Mod1+s exec ~/.local/bin/rofi-pulse-sink.sh
	#Mod1+c exec rofi-pulse-select source

	# Screen brightness
	XF86MonbrightnessUp exec doas brightnessctl set 10%+ | sed -En 's/.*\(([0-9]+)%\).*/\1/p' > $WOBSOCK
	Mod1+shift+e exec doas brightnessctl set 10%+ | sed -En 's/.*\(([0-9]+)%\).*/\1/p' > $WOBSOCK
	XF86MonbrightnessDown exec doas brightnessctl set 5%- | sed -En 's/.*\(([0-9]+)%\).*/\1/p' > $WOBSOCK
	Mod1+shift+n exec doas brightnessctl set 5%- | sed -En 's/.*\(([0-9]+)%\).*/\1/p' > $WOBSOCK
}

### Output configuration

#output * bg "~/pictures/desktop-backgrounds/c\&h.png" fill
output * bg "~/pictures/desktop-backgrounds/solar-system.jpg" fill
#output * bg "~/pictures/desktop-backgrounds/clew-simple.jpg" fill
output * scale 2

bindsym $mod+Shift+r output eDP-1 transform 90 anticlockwise

# You can get the names of your outputs by running: swaymsg -t get_outputs

### Idle configuration

exec swayidle -w \
	timeout 1800 'swaylock -f -c 000000' \
	timeout 3600 'swaymsg "output * dpms off"' resume 'swaymsg "output * dpms on"' \
	before-sleep 'swaylock -f -c 000000'

### Input configuration
input 2362:628:PIXA3854:00_093A:0274_Mouse {
	pointer_accel 0.5
}

input * {
	dwt enabled
	tap enabled
	middle_emulation enabled

	xkb_layout us
	xkb_options compose:caps
	xkb_variant altgr-intl
}

input 18193:2069:mouseless {
	xkb_variant colemak
}
#
# You can get the names of your inputs by running: swaymsg -t get_inputs
# Read `man 5 sway-input` for more information about this section.

# Drag floating windows by holding down $mod and left mouse button.
# Resize them with right mouse button + $mod.
# Despite the name, also works for non-floating windows.
# Change normal to inverse to use left mouse button for resizing and right
# mouse button for dragging.
floating_modifier $mod normal

#
# Resizing containers:
#
mode "resize" {
	# left will shrink the containers width
	# right will grow the containers width
	# up will shrink the containers height
	# down will grow the containers height
	bindsym {
		$left resize shrink width 10px
		$down resize grow height 10px
		$up resize shrink height 10px
		$right resize grow width 10px

		# Ditto, with arrow keys
		Left resize shrink width 10px
		Down resize grow height 10px
		Up resize shrink height 10px
		Right resize grow width 10px

		# Return to default mode
		Return mode "default"
		Escape mode "default"
	}
}
bindsym $mod+r mode "resize"

# Window label colors
client.focused #689d6add #689d6add #1d2021
client.unfocused #f9f5d744 #f9f5d744 #1d2021
client.focused_inactive #458588dd #458588dd #1d2021
client.urgent #d65d0e #d65d0e #1d2021
#client.focused #f9f5d7dd #f9f5d7dd #1d2021
#client.unfocused #00000000 #00000000 #f9f5d7
#client.focused_inactive #f9f5d788 #f9f5d788 #1d2021

#default_border none
#default_floating_border none
font pango:monospace 0
titlebar_padding 6
#titlebar_border_thickness 0

# Gaps
gaps inner 8

# Layout
workspace_layout tabbed

#
# Status Bar:
#
# Read `man 5 sway-bar` for more information about this section.
bar {
	id main
	position bottom
	gaps 8
	tray_padding 10
	font pango:monospace 10
	#mode hide
	#modifier $mod

	tray_output none

	# When the status_command prints a new line to stdout, swaybar updates.
	status_command bumblebee-status

	colors {
		background #f9f5d7
		inactive_workspace #ebdbb2 #ebdbb2 #1d2021
		focused_workspace #689d6a #689d6a #1d2021
		urgent_workspace #d65d0e #d65d0e #1d2021
	}
}
