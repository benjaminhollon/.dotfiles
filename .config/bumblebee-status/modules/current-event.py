"""Short description in RST format

   please have a look at other modules, this will go into the
   documentation verbatim (list of modules)
"""

import core.module
import core.widget

import util.cli

class Module(core.module.Module):
    def __init__(self, config, theme):
        super().__init__(config, theme, core.widget.Widget(self.full_text))

    def hidden(self):
        return self.__hidden

    def full_text(self, widgets):
        result = util.cli.execute("khal --no-color at -df '' -f '{title}' now").strip()
        if len(result) is 0:
            self.__hidden = True
        else:
            self.__hidden = False
        return result

# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4
