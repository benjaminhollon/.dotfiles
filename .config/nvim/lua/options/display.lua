-- indentation rules
vim.opt.tabstop = 4
vim.opt.shiftwidth = 4
vim.opt.autoindent = true
vim.opt.smartindent = true

-- use hybrid line numbers
vim.opt.number = true
vim.opt.relativenumber = true -- this was mindblowing to me when first learning vim

-- folding
vim.opt.foldmethod = 'indent' -- automatically fold code based on indentation

-- lualine
local function getWords()
	if vim.bo.filetype == 'markdown' or vim.bo.filetype == 'vimwiki' or vim.bo.filetype == 'typst' then
		return tostring(vim.fn.wordcount().words) .. ' words'
	else
		return ''
	end
end
require('lualine').setup {
	sections = {
		lualine_a = {'mode'},
		lualine_b = {'branch', 'diff'},
		lualine_c = {'filename'},
		lualine_x = {'filetype'},
		lualine_y = {getWords},
		lualine_z = {'progress'}
	}
}
