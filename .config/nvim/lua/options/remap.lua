-- plugin initialization
require('leap').create_default_mappings()

-- working with outlines
vim.keymap.set('i', 'n<tab>', '<esc>mzI<tab><esc>`zla') -- adds indentation to beginning of line from insert mode
vim.keymap.set('i', 'u<tab>', '<esc>mzF<tab>x`za') -- removes indentation from beginning of line from insert mode

-- saving/closing
vim.keymap.set('n', '<C-s>', ':wa<enter>:<enter>') -- saves all buffers
vim.keymap.set('n', '<C-c>', ':q<enter>') -- closes neovim
vim.keymap.set('i', '<C-s>', '<esc>:wa<enter>a') -- save all buffers from insert mode
vim.keymap.set('i', '<C-c>', '<esc>:q<enter>') -- closes neovim from insert mode

-- folds
vim.keymap.set('n', '<enter>', 'za') -- toggles whether a fold is open

-- navigation
vim.keymap.set('n', '<C-p>', ':bp<enter>') -- previous buffer
vim.keymap.set('n', '<C-n>', ':bn<enter>') -- next buffer
vim.keymap.set('n', '<leader>e', ':Ex<enter>') -- open file browser
vim.keymap.set('n', '<leader>tf', ':Telescope find_files<enter>') -- find files with telescope
vim.keymap.set('n', '<leader>tg', ':Telescope live_grep<enter>') -- telescope ripgrep

-- searches
vim.keymap.set('n', '<leader>n', ':nohl<enter>') -- clears highlighted results because it drives me crazy after I've found what I'm looking for

-- working with matching parens
vim.keymap.set('n', 'd<enter>', 'd%') -- deletes the paren
vim.keymap.set('n', 'c<enter>', 'c%') -- changes the paren
vim.keymap.set('n', 'y<enter>', 'y%') -- yanks the paren

-- focus on writing
vim.keymap.set('n', '<leader>f', ':Twilight<enter>:ZenMode<enter>') -- yanks the paren

-- clipboard
vim.keymap.set('n', '<leader>y', '"+y') -- yanks the paren
vim.keymap.set('n', '<leader>Y', '"+Y') -- yanks the paren

-- spellcheck language
vim.keymap.set('n', '<leader>sf', ':set spelllang=fr<enter>') -- changes spellcheck language to french
vim.keymap.set('n', '<leader>se', ':set spelllang=en_us<enter>') -- changes spellcheck language to US english
vim.keymap.set('n', '<leader>s', ':set spell!<enter>') -- toggles spellcheck
