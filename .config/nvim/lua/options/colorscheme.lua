vim.cmd("colorscheme gruvbox")
vim.o.background = "light"
vim.cmd("let g:airline_theme='base16_gruvbox_light_hard'")
