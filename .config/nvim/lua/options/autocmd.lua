vim.api.nvim_create_autocmd( "FileType", {
	pattern = "markdown", -- applied to markdown files
	callback = function(args)
		vim.opt_local.spell = true -- turn on spellcheck
		vim.opt_local.foldmethod = 'manual' -- don't automatically fold indentation; with markdown I prefer to fold manually if at all
		vim.cmd(':SoftPencil') -- start Paper, to add writing-related tweaks
	end
})

vim.api.nvim_create_autocmd( "FileType", {
	pattern = "mail", -- for when I'm editing my emails
	callback = function(args)
		vim.opt_local.spell = true -- turn on spellcheck

		vim.opt_local.fo = 'awq'
	end
})

vim.api.nvim_create_autocmd( "BufEnter", {
	pattern = "/tmp/tut*", -- for tut messages
	callback = function(args)
		vim.opt.filetype = 'markdown' -- these don't have a file extension but are interpreted as markdown by GtS, so I want the same treatment in my editor
		-- tut-specific remappings
		vim.keymap.set('i', 'yq', '<esc>jdG:wq<enter>') -- deletes the quote of the message being replied to and closes nvim
	end
})

vim.api.nvim_create_autocmd( "BufEnter", {
	pattern = "*.typst", -- for typst documents
	callback = function(args)
		vim.opt.filetype = 'typst'
		vim.cmd(':SoftPencil') -- start Paper, to add writing-related tweaks
	end
})

vim.api.nvim_create_autocmd( "CmdlineLeave", { -- this stops the most recent command from constantly showing up at the bottom of the screen
	callback = function(args)
		vim.cmd('echo ""')
	end
})
