vim.g.vimwiki_list = {
	{path = '~/notes', syntax = 'markdown', ext = '.md'},
	{path = '~/projects/tick/notes', syntax = 'markdown', ext = '.md'},
	{path = '~/writing/poetry', syntax = 'markdown', ext = '.md'}
}
vim.g.vimwiki_global_ext = 0
