-- smartcase - makes it where only explicitly-capitalized letters are case sensitive
vim.opt.ignorecase = true
vim.opt.smartcase = true

-- neovim title - this changes the terminal window's title; I use this sometimes for time tracking based on what I'm editing
vim.opt.title = true

-- signs - needed for gitgutter
vim.opt.signcolumn = 'auto'

-- updates - I use this to make gitgutter update more often
vim.opt.updatetime = 100

-- enable spellcheck by default
vim.opt.spell = true
vim.opt.spelllang = 'en'

-- autotag
require('nvim-ts-autotag').setup({
	opts = {
		enable_close = true, -- Auto close tags
		enable_rename = true, -- Auto rename pairs of tags
		enable_close_on_slash = false -- Auto close on trailing </
	}
})
