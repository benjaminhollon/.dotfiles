return {
	-- aesthetics
	{
		'ellisonleao/gruvbox.nvim', -- I do love this theme. ;)
		opts = {
			contrast = 'hard'
		}
	},
	'ap/vim-css-color', -- highlights color names with said color
	'nvim-lualine/lualine.nvim',
	'nvim-treesitter/nvim-treesitter',

	-- navigation
	'ggandor/leap.nvim', -- super-easy jumping around a file based on two-char sequences
	{
		'nvim-telescope/telescope.nvim', -- a bunch of file grepping, finding stuff
		dependencies = {
			'nvim-lua/plenary.nvim',
			'BurntSushi/ripgrep',
		}
	},

	-- code
	{
		'numToStr/Comment.nvim', -- for manipulating comments in code files
		opts = {
		},
		lazy = false
	},
	{
		'airblade/vim-gitgutter', -- adds a statusline next to the line numbers showing which lines are added, removed, changed, etc.
		dependencies = {
			'nvim-tree/nvim-web-devicons'
		},
	},
	'windwp/nvim-ts-autotag',

	-- programming languages
	'alaviss/nim.nvim',

	-- writing
	'SidOfc/mkdx', -- markdown editing
	{
		'folke/twilight.nvim', -- line-focus
		opts = {
			dimming = {
				alpha = 0.40
			},
			context = 1
		}
	},
	'vimwiki/vimwiki', -- I use this for managing my notes
	{
		'folke/zen-mode.nvim',
		opts = {
			window = {
				backdrop = 1,
				width = 70,
				height = 0.7,
				options = {
					relativenumber = false,
					number = false,
				}
			}
		}
	}, -- moves the page to a central column which I find better for writing
	'kblin/vim-fountain', -- syntax highlighting for fountain, a format for screenplays
	'preservim/vim-pencil', -- tweaks to vim for writers
}
