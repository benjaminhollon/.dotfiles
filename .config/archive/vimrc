" Get the defaults that most users want.
source $VIMRUNTIME/defaults.vim

" Put these in an autocmd group, so that we can delete them easily.
augroup vimrcEx
	au!

	" For all text files set 'textwidth' to 78 characters.
	autocmd FileType text setlocal textwidth=78
augroup END

" Add optional packages.
"
" The matchit plugin makes the % command work better, but it is not backwards compatible.
" The ! means the package won't be loaded right away but when plugins are loaded during initialization.
if has('syntax') && has('eval')
	packadd! matchit
endif

" BELOW THIS POINT BEGINS CONFIG I MADE MYSELF

" Indentation rules; automatically indent, using tabs, displayed with a width
" of two chars per tab
set tabstop=2
set shiftwidth=2
set autoindent
set smartindent

" Break lines in between words
set linebreak

" Spellchecking automatically in markdown files
set spellfile=~/.vim/spell/en.utf-8.add
autocmd FileType markdown setlocal spell

" Use hybrid line numbers
set relativenumber number

" vim-plug
call plug#begin()
	Plug 'vim-airline/vim-airline'
	Plug 'alvan/vim-closetag'
	Plug 'justinmk/vim-sneak'
	Plug 'jasonccox/vim-wayland-clipboard'
	Plug 'morhetz/gruvbox'
	Plug 'ap/vim-css-color'
	Plug 'mattn/emmet-vim'
call plug#end()

" Theme: Everforeset - Light - Hard
if has('termguicolors')
	set termguicolors
	let &t_8f = "\<Esc>[38:2:%lu:%lu:%lum"
	let &t_8b = "\<Esc>[48:2:%lu:%lu:%lum"
endif

let g:sneak#label = 1

autocmd vimenter * ++nested colorscheme gruvbox
set background=light

" Enable native mouse support
set mouse=a
set ttymouse=sgr

" Folding
if &filetype != 'markdown'
	set foldmethod=indent
endif
autocmd FileType markdown setlocal foldmethod=manual
nnoremap <space> za

" Fix Y
nnoremap Y y$

" Insert-mode mappings
inoremap kj <esc>:wq<enter>
inoremap kq <esc>jdG:wq<enter>
inoremap fj <esc>o
inoremap jf <esc>O
inoremap dj <esc>o- 
inoremap n<tab> <esc>mzI<tab><esc>`zla
inoremap u<tab> <esc>mzF<tab>x`za
inoremap ;; <esc>mzA;<esc>`za
