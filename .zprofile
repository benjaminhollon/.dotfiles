if [ "$(hostname)" = "archimedes" ]; then
	pulseaudio --start &
	udevadm monitor -u | while read line; do printf '%s' "${line}" | grep -qE 'remove.*hci[0-9]+.*bluetooth' && playerctl pause; done &

	mkdir -p $XDG_CACHE_HOME

	[ "$(tty)" = "/dev/tty1" ] && exec sway
fi
